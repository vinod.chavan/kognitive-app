import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  headers: any;
  baseUrl = '';
  constructor(public http: HttpClient) {
    this.baseUrl = environment.baseUrl;
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('apptoken', 'r0MrA268ORAobX53qkoaohaA7g9ek3JJ');
  }

  isValidUser(payload: any): Observable<any> {
    const data = {
      email: payload.userName,
      id_type: 'email',
      password: payload.password,
      tenantid: 3
    };
    return this.http.post(`${this.baseUrl}api/v1/login/password`, data, { headers: this.headers });
  }
}
