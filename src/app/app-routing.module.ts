import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLoginComponent } from './app-login/app-login.component';
import { TasklistComponent } from './tasklist/tasklist.component';
import { AuthGuard } from './common/services/authguard';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: AppLoginComponent
  },
 {
    path: 'task-list',
    component: TasklistComponent,
    canActivate: [ AuthGuard ],
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
