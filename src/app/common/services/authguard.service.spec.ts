import { TestBed } from '@angular/core/testing';
import { AuthGuard } from './authguard';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';


describe('AuthGuard', () => {
  let service: AuthGuard;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
      ]
    });
    service = TestBed.inject(AuthGuard);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('calling canActivate should check for authentication', () => {
    spyOn(service, 'canActivate').and.callThrough();
    const snapshot = { url: '' } as RouterStateSnapshot;
    const check = service.canActivate(new ActivatedRouteSnapshot(), snapshot);
    expect(service.canActivate).toHaveBeenCalled();
  });

  it('calling canActivate should not check for authentication', () => {
    service.authenticationService.login({ username: 'admin', password: 'password11' });
    spyOn(service, 'canActivate').and.callThrough();
    const snapshot = { url: '' } as RouterStateSnapshot;
    const check = service.canActivate(new ActivatedRouteSnapshot(), snapshot);
    expect(check).toBe(true);
  });

  it('calling canActivate should not check for authentication with other user', () => {
    service.authenticationService.login({ username: 'coreuser', password: 'password11' });
    spyOn(service, 'canActivate').and.callThrough();
    const snapshot = { url: '' } as RouterStateSnapshot;
    const check = service.canActivate(new ActivatedRouteSnapshot(), snapshot);
    expect(check).toBe(true);
  });
});
