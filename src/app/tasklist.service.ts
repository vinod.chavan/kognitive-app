import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from './common/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TasklistService {
  headers: any;
  baseUrl = '';
  constructor(public http: HttpClient, public auth: AuthenticationService) {
    this.baseUrl = environment.baseUrl;
    const user = this.auth.currentUserValue;
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('apptoken', 'r0MrA268ORAobX53qkoaohaA7g9ek3JJ')
      .set('usertoken', user.user_token);
  }

  getTaskList() {
    const data = {};
    return this.http.post(`${this.baseUrl}api/v1/task/list`, data, { headers: this.headers });
  }
}
