import { Component, OnInit } from '@angular/core';
import { TasklistService } from '../tasklist.service';
import { NotificationService } from '../common/services/notification.service';

@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.scss']
})
export class TasklistComponent implements OnInit {
  tasklist = [];
  showLoader = false;
  constructor(public tasklistService: TasklistService, public notification: NotificationService) {}

  ngOnInit(): void {
    this.showLoader = true;
    this.tasklistService.getTaskList().subscribe((res) => {
      this.tasklist = res['data'] ? res['data'] : [];
      this.showLoader = false;
    }, err => {
      this.notification.showError('Failed to load task list.');
      this.showLoader = false;
    });
  }

}
