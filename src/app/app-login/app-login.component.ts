import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoginService } from '../login.service';
import { AuthenticationService } from '../common/services/authentication.service';
import { Router } from '@angular/router';
import { NotificationService } from '../common/services/notification.service';

@Component({
  selector: 'app-app-login',
  templateUrl: './app-login.component.html',
  styleUrls: ['./app-login.component.scss']
})
export class AppLoginComponent implements OnInit {
  loginForm: FormGroup;
  showLoader = false;

  constructor(public formBuilder: FormBuilder,
    public loginService: LoginService, public auth: AuthenticationService,
    public router: Router, public notification: NotificationService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      password: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  onLoginSubmit() {
    this.showLoader = true;
    const loginPayload = {
      userName: this.loginForm.get('username').value.toLowerCase(),
      password: this.loginForm.get('password').value
    };
   // this.router.navigate(['/task-list']);
    this.loginService.isValidUser(loginPayload).subscribe((res) => {
      this.auth.login(res);
      this.showLoader = false;
      this.router.navigate(['/task-list']);
    }, err => {
      this.showLoader = false;
      this.notification.showError('Invalid username or password.');
    });
  }
}
